{
  description = "Tagged data structures in Julia";

  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";

  outputs = { self, flake-utils, nixpkgs }:
  flake-utils.lib.eachSystem [ flake-utils.lib.system.x86_64-linux ]
  (system: let
    pkgs = nixpkgs.legacyPackages.${system};
  in {
    devShell = pkgs.mkShell {
      packages = with pkgs; [
        julia
        just
      ];
      JULIA_PROJECT = "@.";
    };
  });
}
