module TaggedData

using Underscores

export TagDict, wrap, subset

struct TagDict{K,V} <: AbstractDict{Set{K}, V}
    content::Dict{Set{K},V}
end

function TagDict{K,V}() where {K,V}
    TagDict{K,V}(Dict())
end

# providing default values for K,V if none are given
# not sure why this is needed
function TagDict()
    TagDict{Any,Any}(Dict())
end

function TagDict(arg_pairs::Pair...)
    Dict(
        Iterators.flatten(
            # flatten nested TagDicts given in arg_pairs
            v isa TagDict ? pairs(wrap(v, Set(k))) : [Set(k) => v]
            for (k, v) in arg_pairs
        )
    ) |> TagDict
end

# fallback definition for generators and iterables
function TagDict(kv)
    TagDict(kv...)
end

"""
    wrap(td::TagDict, keys...)

Wrap entries in `td` with additional `keys`.
"""
function wrap(td::TagDict, keys...)
    wrap(td, Set(keys))
end

function wrap(td::TagDict, keyset::Set)
    TagDict(keyset ∪ k => v for (k, v) in td)
end

function Base.merge(td::TagDict, other::AbstractDict...)
    res = TagDict(td)
    for d in other
        for (k, v) in d
            res[k] = v
        end
    end
    res
end

"""
    function subset(td::TagDict, keyset::Set)

Return a TagDict containing only entries which are supersets of `keyset`. Keys
in `keyset` are not present in the resulting TagDict anymore.
"""
function subset(td::TagDict, keyset::Set)
    filtered_pairs = [
        setdiff(k, keyset) => v
        for (k, v) in td
        if keyset ⊆ k
    ]
    TagDict(filtered_pairs...)
end

function subset(td::TagDict, keys...)
    subset(td, Set(keys))
end

### ↓ IMPL iterator

function Base.getindex(td::TagDict, keyset::Set)
    if keyset in keys(td.content)
        # on direct hits, return the saved scalar
        return td.content[keyset]
    else
        subset(td, keyset)
    end
end

"""
    Base.getindex(td::TagDict, keys...)

Filter entries in `td` by `keys`, subtracting `keys` from the keys in
`td.content`.
"""
function Base.getindex(td::TagDict, keys...)
    getindex(td, Set(keys))
end

function Base.get(td::TagDict, key::Set, default)
    get(td.content, key, default)
end

function Base.setindex!(td::TagDict, val, key::Set)
    setindex!(td.content, val, key)
end

function Base.setindex!(td::TagDict, val, keys...)
    setindex!(td.content, val, Set(keys))
end

function Base.setindex!(td::TagDict, val::TagDict, keyset::Set)
    for (k, v) in val
        td[keyset ∪ k] = v
    end
    val
end

function Base.setindex!(td::TagDict, val::TagDict, keys...)
    setindex!(td, val, Set(keys))
end

function Base.iterate(td::TagDict)
    iterate(td.content)
end

function Base.iterate(td::TagDict, state)
    iterate(td.content, state)
end

function Base.length(td::TagDict)
    length(td.content)
end

function Base.eltype(td::TagDict{K,V}) where {K,V}
    Pair{Set{K}, V}
end

end # module TaggedData
