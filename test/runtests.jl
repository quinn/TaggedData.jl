using TaggedData
using Test

@testset "trivial" begin
    # == Indexing, Construction
    td1 = TagDict{Symbol,String}()
    td1[:B, :C] = "ABC"
    td1[:D] = "AD"
    td2 = wrap(td1, :A)
    td2[:foo] = "foo"
    # td2:
    # [:A, :B, :C] => "ABC"
    # [:A, :D] => "AD"
    # [:foo] => "foo"
    @test td2[:foo] == "foo"  # indexing with complete tags
    @test td2[:A] == td1  # indexing returns subset
    @test td2[:A, :B, :C] == "ABC"  # exact indexing with multiple args
    @test td2[:A][:B][:C] == "ABC"  # indexing multiple times
    # == Merging
    td3 = merge(td1, TagDict([:B, :E] => "BE"))
    @test td3 == TagDict([:D] => "AD", [:B, :E] => "BE", [:B, :C] => "ABC")
    # == Setting value to TagDict
    td4 = TagDict()
    td4[:A] = td1
    @test td4 == wrap(td1, :A)
    # == TagDicts are flattened in constructor
    td4 = TagDict([:A] => td1)
    @test td4 == wrap(td1, :A)
    # == Wrap and Subset are inverse operations
    @test subset(wrap(td1, :A), :A) == td1
end
