# TaggedData.jl

Tagged data structures for Julia.

## Ideas

- Nested `TagDict`s don't really make sense. Operations can therefore implicitly
flatten these structures for convenience.

## Examples

Indexing will either result in a scalar or a TagDict, depending on if indexing
is complete.

```julia
using TaggedData

d = TagDict{Symbol,Any}()
d[:minmax] = 1
d[:ft] = 2
d[:mini] = 3
d = wrap(d, :endpoint, :parab)
d[:parab, :mini, :start] = 4

d
# TagDict{Symbol, Int64} with 3 entries:
#   Set([:endpoint, :ft, :parab])     => 2
#   Set([:endpoint, :parab, :mini])   => 3
#   Set([:endpoint, :minmax, :parab]) => 1
#   Set([:parab, :mini, :start]) => 4

d[:endpoint]
# TagDict{Symbol, Int64} with 3 entries:
#   Set([:minmax, :parab]) => 1
#   Set([:parab, :mini])   => 3
#   Set([:ft, :parab])     => 2

d[:endpoint, :parab, :mini]
# 3

# setting to TagDict will add entries
d[:mini] = TagDict([:parab, :endpoint] => 11, [:parab, :start] => 0)
# TagDict{Symbol, Int64} with 3 entries:
#   Set([:endpoint, :ft, :parab])     => 2
#   Set([:endpoint, :parab, :mini])   => 11
#   Set([:endpoint, :minmax, :parab]) => 1
#   Set([:parab, :mini, :start]) => 0
```
